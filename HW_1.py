# Створити дві змінні first=10, second=30. Вивести на екран результат математичної взаємодії (+)
Cat = 10
Dog = 30
Animals_counter = Cat + Dog

print(Animals_counter)

# Створити дві змінні first=10, second=30. Вивести на екран результат математичної взаємодії (-)
Cat = 10
Dog = 30
Animals_counter = Cat - Dog

print(Animals_counter)

# Створити дві змінні first=10, second=30. Вивести на екран результат математичної взаємодії (*)
Cat = 10
Dog = 30
Animals_counter = Cat * Dog

print(Animals_counter)

# Створити дві змінні first=30, second=10. Вивести на екран результат математичної взаємодії (/)
Cat = 30
Dog = 10
Animals_counter = Cat / Dog

print(Animals_counter)

# Створити дві змінні first=30, second=10. Вивести на екран результат математичної взаємодії (//)
Cat = 30
Dog = 10
Animals_counter = Cat // Dog

print(Animals_counter)

# Створити дві змінні first=30, second=10. Вивести на екран результат математичної взаємодії (%)
Cat = 30
Dog = 10
Animals_counter = Cat % Dog

print(Animals_counter)

# Створити дві змінні first=30, second=10. Вивести на екран результат математичної взаємодії (**10)
Cat = 30
Cats_counter = Cat **10

print(Cats_counter)

# Створити змінну і почергово записати в неї результат порівняння (<) чисел з завдання 1
Cat = 10
Dog = 30
Animals_counter = Cat < Dog

print(Animals_counter)

# Створити змінну і почергово записати в неї результат порівняння (>) чисел з завдання 1
Cat = 10
Dog = 30
Animals_counter = Cat > Dog

print(Animals_counter)

# Створити змінну і почергово записати в неї результат порівняння (==) чисел з завдання 1
Cat = 10
Dog = 30
Animals_counter = Cat == Dog

print(Animals_counter)

# Створити змінну і почергово записати в неї результат порівняння (!=) чисел з завдання 1
Cat = 10
Dog = 30
Animals_counter = Cat != Dog

print(Animals_counter)

# Створити змінну - результат конкатенації (складання) строк str1="Hello " и str2="world"
Word1 = "Hello "
Word2 = "World!"
Result = Word1 + Word2
print(Result)

